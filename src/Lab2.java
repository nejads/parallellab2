import TSim.*;
import java.util.Random;

public class Lab2 {
	private final static int maxspeedTrain = 15;
	private final static int maxspeedSim = 100;
	
	private final Monitor [] monitors;
	private Thread first;
	private Thread second;
	private TSimInterface tsi;

	public static void main(String[] args) {
		Lab2 lab2 = new Lab2(args);
		lab2.start();
	}

	public Lab2(String[] args) {
		tsi = TSimInterface.getInstance();
		int speedSim;
		int speed1;
		int speed2;
		int len = args.length;
		
		//Arguments handling. If speed is missing set a random integer between 1 and maxspeed.
		if(len==0){
			speedSim = maxspeedSim;
			speed1 = randInt();
			speed2 = randInt();
		}
		else if(len==1){
			speedSim = maxspeedSim;
			speed1 = Integer.parseInt(args[0]);
			speed2 = randInt();
		}
		else if(len==2){
			speedSim = maxspeedSim;
			speed1 = Integer.parseInt(args[0]);
			speed2 = Integer.parseInt(args[1]);
		}
		else {
			speedSim = Integer.parseInt(args[2]);
			speed1 = Integer.parseInt(args[0]);
			speed2 = Integer.parseInt(args[1]);
		}
		
		//Set debugger
		tsi.setDebug(false);
		
		//Initiate Monitors
		monitors = new Monitor[] {new Monitor(),new Monitor(),new Monitor(),
				new Monitor(),new Monitor(),new Monitor(),
				new Monitor()};

		//Initiate threads
		first = new Thread(new Train(tsi, speedSim, speed1, 1, monitors));
		second = new Thread(new Train(tsi, speedSim, speed2, 2, monitors));
	}

	private void start() {
		first.start();
		second.start();
	}
	
	private static int randInt() {		
	    Random rand = new Random();
	    int randomNum = rand.nextInt((maxspeedTrain - 1) + 1) + 1;
	    return randomNum;
	}
}
