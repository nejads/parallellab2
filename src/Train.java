import java.awt.Point;

import TSim.*;

public class Train implements Runnable {
	private int trainId;
	private int speed;
	private int speedSim;
	private TSimInterface tsi;
	private Point position;
	private boolean toUp;
	private Monitor[] monitors;
	private boolean[] Sensor;

	public Train(TSimInterface t, int speedSim, int speed, int tId, Monitor[] m) {
		trainId = tId;
		tsi = t;
		this.speed = speed;
		this.speedSim = speedSim;
		this.monitors = m;
		position = new Point(0, 0);
		toUp = trainId == 2 ? true : false;
	}

	@Override
	public void run() {
		start(trainId);
		while (true) {
			try {
				SensorEvent se = tsi.getSensor(trainId);
				if (se.getStatus() == SensorEvent.ACTIVE) {
				if (toUp) { // down-to-up
					position.setLocation(se.getXpos(), se.getYpos()); // Update
																		// the
																		// train
																		// position
					sensorPosition();
					if (Sensor[1]) {
						monitors[1].enter();
						System.err.println("Down-to-up: Monitor 1 has entered");
					} else if (Sensor[2]) {
						//Unnecessary in this case
					} else if (Sensor[3]) {
						stop(trainId);
						monitors[2].enter();
						System.err.println("Down-to-up: Monitor 2 has entered");
						tsi.setSwitch(3, 11, tsi.SWITCH_LEFT);
						start(trainId);
						monitors[1].leave();
						System.err.println("Down-to-up: Monitor 1 has leaved");
					} else if (Sensor[4]) {
						stop(trainId);
						monitors[2].enter();
						System.err.println("Down-to-up: Monitor 2 has entered");
						tsi.setSwitch(3, 11, tsi.SWITCH_RIGHT);
						start(trainId);
					} else if (Sensor[5]) {
						stop(trainId);
						if(monitors[3].tryEnter()){
							System.err.println("Down-to-up: Monitor 3 have entered");
							tsi.setSwitch(4, 9, tsi.SWITCH_LEFT);
							start(trainId);
						} else {
							System.err.println("Down-to-up: Monitor 3 have NOT entered");
							tsi.setSwitch(4, 9, tsi.SWITCH_RIGHT);
							start(trainId);
						}
					} else if (Sensor[6]) {
						monitors[2].leave();
						System.err.println("Down-to-up: Monitor 2 has leaved");
					} else if (Sensor[7]) {
						monitors[2].leave();
						System.err.println("Down-to-up: Monitor 2 has leaved");
					} else if (Sensor[8]) {
						stop(trainId);
						monitors[4].enter();
						System.err.println("Down-to-up: Monitor 4 have entered");
						tsi.setSwitch(15, 9, tsi.SWITCH_RIGHT);
						start(trainId);
						monitors[3].leave();
						System.err.println("Down-to-up: Monitor 3 has leaved");
					} else if (Sensor[9]) {
						stop(trainId);
						monitors[4].enter();
						System.err.println("Down-to-up: Monitor 4 have entered");
						tsi.setSwitch(15, 9, tsi.SWITCH_LEFT);
						start(trainId);
					} else if (Sensor[10]) {
						stop(trainId);
						if(monitors[5].tryEnter()){
							System.err.println("Down-to-up: Monitor 5 have entered");
							tsi.setSwitch(17, 7, tsi.SWITCH_RIGHT);
							start(trainId);
						} else {
							System.err.println("Down-to-up: Monitor 5 have NOT entered");
							tsi.setSwitch(17, 7, tsi.SWITCH_LEFT);
							start(trainId);
						}
					} else if (Sensor[11]) {
						monitors[4].leave();
						System.err.println("Down-to-up: Monitor 4 has leaved");
					} else if (Sensor[12]) {
						monitors[4].leave();
						System.err.println("Down-to-up: Monitor 4 has leaved");
					} else if (Sensor[13]) {
						stop(trainId);
						monitors[6].enter();
						System.err.println("Down-to-up: Monitor 6 have entered");
						start(trainId);
					} else if (Sensor[14]) {
						stop(trainId);
						monitors[6].enter();
						System.err.println("Down-to-up: Monitor 6 have entered");
						start(trainId);
					} else if (Sensor[15]) {
						monitors[6].leave();
						System.err.println("Down-to-up: Monitor 6 has leaved");
					} else if (Sensor[16]) {
						monitors[6].leave();
						System.err.println("Down-to-up: Monitor 6 has leaved");
					} else if (Sensor[17]) {
						station(trainId);
					} else if (Sensor[18]) {
						station(trainId);
					}
				} else { // up-to-down
						position.setLocation(se.getXpos(), se.getYpos());
						sensorPosition();
						if (Sensor[18]) {
							System.err.println("I'm starting to go down");
							monitors[5].enter();
							System.err.println("Up-to-down:  Monitor 5 have entered");
						} else if (Sensor[17]) {
							//Unnecessary in this case
						} else if (Sensor[16]) {
							stop(trainId);
							monitors[6].enter();
							System.err.println("Up-to-down: Monitor 6 have entered");
							start(trainId);
						} else if (Sensor[15]) {
							stop(trainId);
							monitors[6].enter();
							System.err.println("Up-to-down: Monitor 6 have entered");
							start(trainId);
						} else if (Sensor[14]) {
							monitors[6].leave();
							System.err.println("Up-to-down: Monitor 6 has leaved");
						} else if (Sensor[13]) {
							monitors[6].leave();
							System.err.println("Up-to-down: Monitor 6 has leaved");
						} else if (Sensor[12]) {
							stop(trainId);
							monitors[4].enter();
							System.err.println("Up-to-down: Monitor 4 have entered");
							tsi.setSwitch(17, 7, tsi.SWITCH_LEFT);
							start(trainId);
						} else if (Sensor[11]) {
							stop(trainId);
							monitors[4].enter();
							System.err.println("Up-to-down: Monitor 4 have entered");
							tsi.setSwitch(17, 7, tsi.SWITCH_RIGHT);
							start(trainId);
							monitors[5].leave();
							System.err.println("Up-to-down: Monitor 5 has leaved");
						} else if (Sensor[10]) {
							stop(trainId);
							if (monitors[3].tryEnter()){ 
								System.err.println("Up-to-down:  Monitor 3 have entered");
								tsi.setSwitch(15, 9, tsi.SWITCH_RIGHT);
								start(trainId);
							} else {
								System.err.println("Up-to-down:  Monitor 3 have NOT entered");
								tsi.setSwitch(15, 9, tsi.SWITCH_LEFT); 
								start(trainId);
							}
						} else if (Sensor[9]) {
							monitors[4].leave();
							System.err.println("Up-to-down: Monitor 4 has leaved");
						} else if (Sensor[8]) {
							monitors[4].leave();
							System.err.println("Up-to-down: Monitor 4 has leaved");
						} else if (Sensor[7]) {
							stop(trainId);
							monitors[2].enter();
							System.err.println("Up-to-down:  Monitor 2 have entered");
							tsi.setSwitch(4, 9, tsi.SWITCH_RIGHT);
							start(trainId);
						} else if (Sensor[6]) {
							stop(trainId);
							monitors[2].enter();
							System.err.println("Up-to-down:  Monitor 2 have entered");
							tsi.setSwitch(4, 9, tsi.SWITCH_LEFT);
							start(trainId);
							monitors[3].leave();
							System.err.println("Up-to-down: Monitor 3 has leaved");
						} else if (Sensor[5]) {
							stop(trainId);
							if (monitors[1].tryEnter()){
								System.err.println("Up-to-down:  Monitor 1 have entered");
								tsi.setSwitch(3, 11, tsi.SWITCH_LEFT);
								start(trainId);
							} else {
								System.err.println("Up-to-down:  Monitor 1 have NOT entered");
								tsi.setSwitch(3, 11, tsi.SWITCH_RIGHT);
								start(trainId);
							}
						} else if (Sensor[4]) {
							monitors[2].leave();
							System.err.println("Up-to-down:  monitor 2 have leaved");
						} else if (Sensor[3]) {
							monitors[2].leave();
							System.err.println("Up-to-down:  Monitoer 2 have leaved");
						} else if (Sensor[2]) { //station
							station(trainId);
						} else if (Sensor[1]) { //station
							station(trainId);
						} 
					

				} }
			} catch (CommandException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void start(int tId) {
		try {
			tsi.setSpeed(tId, speed);
		} catch (CommandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void stop(int tId) {
		try {
			tsi.setSpeed(tId, 0);
		} catch (CommandException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void station(int tId) {
		stop(trainId);
		try {
			Thread.sleep(500 + 2 * speedSim * Math.abs(speed));
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		toUp = !toUp;
		speed = -1 * speed;
		start(trainId);
	}

	private void sensorPosition() {
		Sensor = new boolean[] {
				position.getX() == 0 && position.getY() == 0 ? true : false,
				position.getX() == 14 && position.getY() == 11 ? true : false,	//1- station (down - up)
				position.getX() == 15 && position.getY() == 13 ? true : false,	//2- station (down - down)
				position.getX() == 6 && position.getY() == 11 ? true : false,	//3
				position.getX() == 6 && position.getY() == 13 ? true : false,	//4
				position.getX() == 1 && position.getY() == 10 ? true : false,	//5 - left intersection
				position.getX() == 7 && position.getY() == 9 ? true : false,	//6
				position.getX() == 7 && position.getY() == 10 ? true : false,	//7
				position.getX() == 12 && position.getY() == 9 ? true : false,	//8
				position.getX() == 12 && position.getY() == 10 ? true : false,	//9
				position.getX() == 19 && position.getY() == 8 ? true : false,	//10- right intersection
				position.getX() == 14 && position.getY() == 7 ? true : false,	//11
				position.getX() == 14 && position.getY() == 8 ? true : false,	//12
				position.getX() == 10 && position.getY() == 7 ? true : false,	//13 - crossing
				position.getX() == 10 && position.getY() == 8 ? true : false,	//14 - crossing
				position.getX() == 6 && position.getY() == 6 ? true : false,	//15 - crossing
				position.getX() == 9 && position.getY() == 5 ? true : false,	//16 - crossing
				position.getX() == 15 && position.getY() == 5 ? true : false,	//17 - station (up - down)
				position.getX() == 14 && position.getY() == 3 ? true : false, };//18 - station(up - up)
	}
}
