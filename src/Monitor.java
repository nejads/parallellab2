import java.util.concurrent.locks.*;

public class Monitor {

	private final Lock lock = new ReentrantLock();
	private final Condition unoccupied = lock.newCondition();
	private boolean isOccupied = false;

	public Monitor() {
		super();
	}

	public void enter(){
	    lock.lock();
	    try {
	        while(isOccupied)
	            unoccupied.await();
	        isOccupied = true;
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    } finally {
	        lock.unlock();
	    }
	}

	public boolean tryEnter() {
		if (isOccupied) {
			return false;
		} else {
			enter();
			return true;
		}
	}

	public void leave(){
	    lock.lock();
	    try {
	        isOccupied = false;
	        unoccupied.signal();
	    } finally {
	        lock.unlock();
	    }
	}

}